![re:claimID](https://gitlab.com/reclaimid/ui/raw/master/src/assets/reclaim_icon.png)

# Manage your re:claimID OpenID Connect clients

This tools allows you to manage your OpenID Connect clients in re:claimID.

## Installation
This tool can be installed using rubygems:

```
$ gem install reclaim-oidc
```

## List clients
To list all registered clients

```
$ reclaim-oidc --list
```

## Manage clients:
Add a new client like this:
```
$ reclaim-oidc --add --client-name myclient --redirect-uri https://my.website.tld/authz_cb --description "My Cool Webpage"
```

Delete a client:
```
$ reclaim-oidc --delete --client-name myclient
```

## Change the JSON-Web-Token secret
To change the JWT secret key:
```
$ reclaim-oidc --jwt-secret new_jwt_secret
```
